package chapter5

import org.junit.experimental.theories.DataPoints
import org.junit.experimental.theories.Theory

import static org.junit.Assert.*
import static org.hamcrest.Matchers.*

class CalcTheoriesTest {
    @DataPoints
    static int[][] VALUES = [
        [0, 0, 0],
        [0, 1, 1],
        [1, 0, 1],
        [3, 4, 7],
    ]

    @Theory
    void add(int[] values){
        // FIXME なぜかCalcが見つからなくてコンパイルが通らない
        Calc sut = new Calc()
        assertThat sut.add(values[0], values[1]), is(values[2])
    }

}
