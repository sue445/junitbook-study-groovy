package chapter5

import org.junit.Before
import org.junit.Test
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import static org.junit.Assert.*
import static org.hamcrest.Matchers.*

@RunWith(Enclosed.class)
class ItemStockTest {
    static class 空の場合 {
        ItemStock sut

        @Before
        void setUp(){
            sut = new ItemStock()
        }

        @Test
        void size_Aが0を返す(){
            assertThat sut.size("A"), is(0)
        }

        @Test
        void contains_Aはfalseを返す(){
            assertThat sut.contains("A"), is(false)
        }
    }

    static class 商品Aを1件含む場合 {
        ItemStock sut

        @Before
        void setUp(){
            sut = new ItemStock()
            sut.add("A", 1)
        }

        @Test
        void sizeが1を返す(){
            assertThat sut.size("A"), is(1)
        }

        @Test
        void contains_Aはtrueを返す(){
            assertThat sut.contains("A"), is(true)
        }
    }
}
