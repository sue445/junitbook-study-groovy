package chapter11

import org.junit.Test

import static org.junit.Assert.*
import static org.hamcrest.Matchers.*

/**
 * Created with IntelliJ IDEA.
 * User: sue445
 * Date: 2013/02/02
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
class MethodExtractExampleTest {
    @Test
    void "doSomethingを実行するとdateに現在時刻が設定される"(){
        final Date current = new Date()
        MethodExtractExample sut = new MethodExtractExample(){
            @Override
            Date newDate(){
                return current;
            }
        };

        sut.doSomething();
        assertThat sut.date, is(sameInstance(current))
    }
}
