package chapter11

import org.hamcrest.Matchers
import org.junit.Test
import static org.junit.Assert.*
import static org.hamcrest.Matchers.*

/**
 * Created with IntelliJ IDEA.
 * User: sue445
 * Date: 2013/02/02
 * Time: 18:33
 * To change this template use File | Settings | File Templates.
 */
class RandomsTest {

    @Test
    void "choiceでAを返す"(){
        List<String> options = new ArrayList<String>()
        options.add "A"
        options.add "B"

        Randoms sut = new Randoms()
        sut.generator = new RandomNumberGenerator() {
            @Override
            int nextInt() {
                0
            }
        }

        assertThat sut.choice(options), is("A")
    }
}
