package chapter4

import org.junit.Test

import static chapter4.IsDate.dateOf
import static org.junit.Assert.assertThat
import static org.hamcrest.CoreMatchers.*
import org.junit.Ignore

/**
 * Created with IntelliJ IDEA.
 * User: sue445
 * Date: 2012/12/06
 * Time: 0:49
 * To change this template use File | Settings | File Templates.
 */
class IsDateTest {
    @Ignore
    @Test
    void test(){
        assertThat new Date(), is(dateOf(2012, 1, 12))
    }

    @Ignore
    @Test
    void 日付の比較(){
        Date date = new Date()
        assertThat date, is(dateOf(2011, 2, 10))
    }
}
