package chapter4;


import org.junit.Ignore
import org.junit.Test

import static org.junit.Assert.assertThat
import static org.junit.Assert.fail

class AssertionTest{
    @Test
    void assertion(){
        String actual = "Hello" + " " + "World"
        assertThat actual, is("Hello World")
    }

    @Ignore
    @Test
    void なにか難しいけど重要なテストケース(){
        fail "TODO テストコードを実装する"
    }
}