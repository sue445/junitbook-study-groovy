package chapter11

/**
 * Created with IntelliJ IDEA.
 * User: sue445
 * Date: 2013/02/02
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
class Randoms {
    RandomNumberGenerator generator = new RandomNumberGeneratorImpl()

    public <T> T choice(List<T> options){
        if(options.size() == 0) return null
        int idx = generator.nextInt() % options.size()
        return options.get(idx)
    }
}
