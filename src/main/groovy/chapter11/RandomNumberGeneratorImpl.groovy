package chapter11

/**
 * Created with IntelliJ IDEA.
 * User: sue445
 * Date: 2013/02/02
 * Time: 16:39
 * To change this template use File | Settings | File Templates.
 */
class RandomNumberGeneratorImpl implements RandomNumberGenerator{
    private final Random rand = new Random()

    @Override
    int nextInt() {
        return rand.nextInt()
    }
}
