package chapter4

import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import java.text.SimpleDateFormat

/**
 * Created with IntelliJ IDEA.
 * User: sue445
 * Date: 2012/12/06
 * Time: 0:37
 * To change this template use File | Settings | File Templates.
 */
class IsDate extends BaseMatcher<Date>{

    private final int yyyy
    private final int mm
    private final int dd
    private Object actual

    IsDate(int yyyy, int mm, int dd){
        this.yyyy = yyyy
        this.mm = mm
        this.dd = dd
    }

    public static Matcher<Date> dateOf(int yyyy, int mm, int dd){
        new IsDate(yyyy, mm, dd)
    }

    @Override
    boolean matches(Object actual) {
        this.actual = actual
        if(!(actual instanceof Date)){
            return false
        }

        Calendar cal = Calendar.getInstance()
        cal.setTime((Date)actual)

        if(yyyy != cal.get(Calendar.YEAR)){
            return false
        }
        if(mm != cal.get(Calendar.MONTH)){
            return false
        }
        if(dd != cal.get(Calendar.DATE)){
            return false
        }

        true
    }

    @Override
    void describeTo(Description desc) {
        desc.appendValue String.format("%d/%02d/%02d", yyyy, mm, dd)

//        if(actual != null){
//            desc.appendText " but actual is "
//            desc.appendValue new SimpleDateFormat("yyyy/MM/dd").format((Date)actual)
//        }
    }
}
